# README #

### Description ###
- To implement a custom pie chart with the next features/options:

	1. Configure amount of sections (param - integer, NSInteger), up to 6

	2. Configure color of each section

	3. Configure size of each section (param - integer, NSInteger). 
		Example: 2 sections, 1st == 50, 2nd == 150. 1st section should occupy 25% os space, 2nd respectively - 75%.
		Example: 3 sections, 1st == 50, 2nd == 150, 3rd == 200. 1st 12.5%, 2nd - 37.5%, 3rd - 50%.  

	4. Configure padding between sections (param - px, NSInteger). 

	5. Configure width of sections’ lines (param - px, NSInteger)

	6. Configure partial opacity_size (param - integer, NSInteger) 
		Example: if we have 2 sections - 1st == 50, 2nd == 150 and opacity_size - 80. 40% of the circle would have opacity.

- To create basic UI when we can configure all listed down params

### Language ###
Swift 2.0

### IDE & SDK ###
XCode 7, iOS 9.0

### Deployment target ###
iOS 8.0

### Device support ###

* Support for all iPhone screen sizes
* Portrait orientation

### External Libraries & Dependencies ###

* ios-charts library by Daniel Cohen Gindi. This is the only good choice available for charts in iOS right now.
  https://github.com/danielgindi/ios-charts
* Swift color picker by Christian Zimmermann. 
https://github.com/Christian1313/iOS_Swift_ColorPicker