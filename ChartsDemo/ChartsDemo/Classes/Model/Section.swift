//
//  Section.swift
//  ChartsDemo
//
//  Created by Rajavelu Chandrasekaran on 26/09/15.
//  Copyright © 2015 Rajavelu Chandrasekaran. All rights reserved.
//

import UIKit
import Foundation

class Section: NSObject {
    
    var title:String?
    var size:Int?
    var color:UIColor?
    var tag:Int?
}