//
//  ChartConfigViewController.swift
//  ChartsDemo
//
//  Created by Rajavelu Chandrasekaran on 26/09/15.
//  Copyright © 2015 Rajavelu Chandrasekaran. All rights reserved.
//

import UIKit

class ChartConfigViewController: UIViewController, UIPopoverPresentationControllerDelegate, SwiftColorPickerDelegate , UITextFieldDelegate {
    
    @IBOutlet var sectionCountLabel: UILabel!
    @IBOutlet var sectionCountSlider: UISlider!
    @IBOutlet var configTable: UITableView!
   
    var colorPickerVC: SwiftColorPickerViewController!
    var activeColorButton: UIButton!
    
    var sectionArray:NSMutableArray!
    var sectionCount:Int!
    var tableFrame:CGRect!
    
    // MARK: - View lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Configuration"
        
        sectionArray = NSMutableArray()
        tableFrame = configTable.frame
        sectionCount = Int(sectionCountSlider.value)
        sectionCountLabel.text = "No of Sections: \(sectionCount)"
        
        // Tap gesture recognizer to dismiss numberpad keyboard
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapRecognizer)
        
        // Notifications for keyboard
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillShow:"),
            name: UIKeyboardWillShowNotification,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillHide:"),
            name: UIKeyboardWillHideNotification,
            object: nil)
        
        // initialize the section data
        initializeSectionData()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after the view is rendered, typically from a nib.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        // Do any additional setup after the view is rendered, typically from a nib.
        dismissKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: Navigation delegates
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        dismissKeyboard()
        
        if (segue.identifier == "chartviewcontroller")
        {
            // adding as delegate for color selection
            let chartVC:ChartViewController = segue.destinationViewController as! ChartViewController
            chartVC.sectionArray = sectionArray
        }
    }
    
    // MARK: - Private methods - Keyboard
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            if(configTable.frame.size.height == tableFrame.size.height) {
                var frame = configTable.frame
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationBeginsFromCurrentState(true)
                UIView.setAnimationDuration(0.3)
                frame.size.height = frame.size.height - keyboardSize.height
                configTable.frame = frame
                UIView.commitAnimations()
            }
        }
    }
    
    func keyboardWillHide(note: NSNotification) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.1)
        configTable.frame = tableFrame
        UIView.commitAnimations()
    }
    
    
    // MARK: Other private methods
    func initializeSectionData() {
        for index in 0..<sectionCount {
            let section:Section = Section()
            section.title = "Section \(index+1)"
            section.color = getRandomColor()
            section.tag = index
            section.size = 50
            
            sectionArray.addObject(section)
        }
    }
    
    func getRandomColor() -> UIColor {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        
        return UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
    }

    
    // MARK: - Textfield delegates
    func textFieldDidEndEditing(textField: UITextField) {
        let section:Section = sectionArray.objectAtIndex(textField.tag) as! Section
        if(textField.text != "") {
            section.size = Int(textField.text!)
        }else{
            section.size = 0
        }
    }
    
    // MARK: - Slider actions
    @IBAction func sliderValueChanged(sender: UISlider) {
        
        sectionCount = Int(sender.value)
        sectionCountLabel.text = "No of Sections: \(sectionCount)"
        
        if(sectionCount > sectionArray.count) {
                let section:Section = Section()
                section.title = "Section \(sectionArray.count+1)"
                section.tag = sectionArray.count
                section.color = getRandomColor()
                section.size = 50
                sectionArray.addObject(section)
        }
        
        if(sectionCount < sectionArray.count) {
            sectionArray.removeLastObject()
        }

        configTable.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableFrame = configTable.frame
        if let _ = sectionArray {
            return sectionArray.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("sectionConfigCell", forIndexPath: indexPath) as! SectionConfigCell
        
        let section:Section = sectionArray.objectAtIndex(indexPath.row) as! Section
        cell.sectionTitle.text = section.title
        cell.colorPickerButton.addTarget(self, action: "colorPickerTapped:", forControlEvents: .TouchUpInside)
        cell.colorPickerButton.tag=section.tag!
        cell.colorPickerButton.backgroundColor = section.color
        cell.sizeTextField.text = "\(section.size!)"
        cell.sizeTextField.tag = section.tag!
        cell.sizeTextField.delegate = self
        
        return cell
    }
    
    // MARK: Table view delegates
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0;
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "Section Configuration"
    }

    
    // MARK: - Popover presentation delegates
    // this enables pop over on iphones
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.None
    }
    
    // MARK: Color Picker delegates and actions
    func colorSelectionChanged(selectedColor color: UIColor) {
        
        activeColorButton.backgroundColor = color;
        
        let section:Section = sectionArray.objectAtIndex(activeColorButton.tag) as! Section
        section.color = color
        
        colorPickerVC.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func colorPickerTapped(sender: UIButton!) {
        
        dismissKeyboard()
        activeColorButton = sender
        
        colorPickerVC = SwiftColorPickerViewController()
        colorPickerVC.delegate = self
        colorPickerVC.modalPresentationStyle = .Popover
        let popVC = colorPickerVC.popoverPresentationController!
        popVC.sourceRect = sender.frame
        popVC.sourceView = self.view
        popVC.permittedArrowDirections = .Any
        popVC.delegate = self
        
        self.presentViewController(colorPickerVC, animated: true, completion:nil)
    }
}
