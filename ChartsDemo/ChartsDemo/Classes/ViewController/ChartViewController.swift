//
//  ChartViewController.swift
//  ChartsDemo
//
//  Created by Rajavelu Chandrasekaran on 26/09/15.
//  Copyright © 2015 Rajavelu Chandrasekaran. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet var widthSlider: UISlider!
    @IBOutlet var paddingSlider: UISlider!
    var pieChartDataSet:PieChartDataSet!
    
    var sectionArray:NSMutableArray!
    var dataPoints:[String] = []
    var dataValues:[Double] = []
    var colors:[UIColor] = []
    var sectionPadding:CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Pie Chart"
        widthSlider.value = 0.4   //setting defaults
        paddingSlider.value = 2.0 //setting defaults
        
        for index in 0..<sectionArray.count {
            let section:Section = sectionArray.objectAtIndex(index) as! Section
            dataPoints.append("\(section.tag!+1)")
            dataValues.append(Double(section.size!))
            colors.append(section.color!)
        }
        
        setChart(dataPoints, values: dataValues)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Rendering the chart
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "")
        pieChartDataSet.colors = colors
        pieChartDataSet.sliceSpace = CGFloat(paddingSlider.value)
        
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        
        pieChartView.holeRadiusPercent = 1.0 - CGFloat(widthSlider.value)
        pieChartView.transparentCircleRadiusPercent = 0.0
        
        // animation
        pieChartView.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
    }
    
    @IBAction func widthChanged(sender: UISlider) {
        pieChartView.holeRadiusPercent = 1.0 - CGFloat(sender.value)
        pieChartView.setNeedsDisplay()
    }
    
    @IBAction func paddingChanged(sender: UISlider) {
        pieChartDataSet.sliceSpace = CGFloat(sender.value)
        pieChartView.setNeedsDisplay()
    }
}

