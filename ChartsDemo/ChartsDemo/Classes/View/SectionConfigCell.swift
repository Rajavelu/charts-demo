//
//  SectionConfigCell.swift
//  ChartsDemo
//
//  Created by Rajavelu Chandrasekaran on 26/09/15.
//  Copyright © 2015 Rajavelu Chandrasekaran. All rights reserved.
//

import UIKit

class SectionConfigCell: UITableViewCell {

    @IBOutlet weak var sizeTextField: UITextField!
    @IBOutlet weak var colorPickerButton: UIButton!
    @IBOutlet weak var sectionTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
